/*
    Create functions which can manipulate our arrays.
*/

let registeredUsers = [

    "James Jeffries",
    "Gunther Smith",
    "Macie West",
    "Michelle Queen",
    "Shane Miguelito",
    "Fernando Dela Cruz",
    "Akiko Yukihime"
];

let friendsList = [];

/*
    
   1. Create a function which will allow us to register into the registeredUsers list.
        - This function should be able to receive a string.
        - Determine if the input username already exists in our registeredUsers array.
            - If it is, show an alert window with the following message:
                "Registration failed. Username already exists!"
            - If it is not, add the new username into the registeredUsers array and show an alert:
                "Thank you for registering!"
        - Invoke and register a new user in the browser console.
        - In the browser console, log the registeredUsers array.

*/
   function register(name){
   	let indexOfName = registeredUsers.indexOf(name);
   		if(name = "undefined"){
			alert("Please enter a name!")  			
   		}else if(indexOfName === -1){
   			alert("Thank you for registering!")
   		}else{
   			alert("Registration Failed. Username already exists!")
   		}
   }

/*
    2. Create a function which will allow us to add a registered user into our friends list.
        - This function should be able to receive a string.
        - Determine if the input username exists in our registeredUsers array.
            - If it is, add the foundUser in our friendList array.
                    -Then show an alert with the following message:
                        - "You have added <registeredUser> as a friend!"
            - If it is not, show an alert window with the following message:
                - "User not found."
        - Invoke the function and add a registered user in your friendsList in the browser console.
        - In the browser console, log the friendsList array in the console.

*/
	function addFriend(name) {
		let indexOfName = registeredUsers.indexOf(name);
			if(indexOfName === -1){
				alert("User Not Found")
			}else{
				let foundUser = name;
				friendsList.push(name)
				alert("You have added "+ foundUser +" as a friend!")
			}
	}

/*
    3. Create a function which will allow us to show/display the items in the friendList one by one on our console.
        - If the friendsList is empty show an alert: 
            - "You currently have 0 friends. Add one first."
        - Invoke the function in the browser console.

*/
	function showFriendList(){
		if(friendsList.length <= 0){
			alert("You currently have 0 friends. Add one first.")
		}else{
			friendsList.forEach(function(friend){
				console.log(friend);
			}); 
		}
	}

/*
    4. Create a function which will display the amount of registered users in your friendsList.
        - If the friendsList is empty show an alert:
            - "You currently have 0 friends. Add one first."
        - If the friendsList is not empty show an alert:
            - "You currently have <numberOfFriends> friends."
        - Invoke the function in the browser console.

*/
	function amountOfFriends(){
		if(friendsList.length <= 0){
			alert("You currently have 0 friends. Add one first.")
		}else{
			console.log("You currently have "+ friendsList.length + " friends.");
		}
	}

/*
    5. Create a function which will delete the last item you have added in the friendsList.
        - If the friendsList is empty show an alert:
            - "You currently have 0 friends. Add one first."
        - Invoke the function in the browser console.
        - In the browser console, log the friendsList array.

*/
	function deleteLast(){
		if(friendsList.length <= 0){
			alert("You currently have 0 friends. Add one first.")
		}else{
			let deletedItem = friendsList.pop();
			alert("Deleted " + deletedItem);
		}
	}

